import pytest
from utils.user.user_type_util import UserType
from users import models

@pytest.mark.django_db
class TestUserTypeUtil:
    def setup_method(self):
        pass


    def test_get_user_type_by_model(self):
        user_model = models.CustomUser
        result = UserType.get_user_type_by_model(user_model)

        assert result == user_model.super_user_type

    def test_get_user_type_by_model_false(self):
        user_model = "false model"
        with pytest.raises(Exception) as error:
            UserType.get_user_type_by_model(user_model)

            assert error == f'Not exist User type by this Model User ({user_model})'


    #CT1
    def test_is_valid_user_type_ct1(self):
        user_type = 'super_user'
        user_model = None

        result = UserType.is_valid_user_type(user_type, user_model)

        assert result == user_type
        

    #CT2
    def test_is_valid_user_type_ct2(self):
        user_type = 'university_admin'
        user_model = models.UniversityUser

        result = UserType.is_valid_user_type(user_type, user_model)

        assert result == user_type

    #CT3
    def test_is_valid_user_type_ct3(self):
        user_type = 'super_user'
        user_model = models.CustomUser

        result = UserType.is_valid_user_type(user_type, user_model)

        assert result == user_type

    #CT4
    def test_is_valid_user_type_ct4(self):
        user_type = 'wrong_user'
        user_model = None

        with pytest.raises(Exception) as error:
            UserType.is_valid_user_type(user_type, user_model)

            assert error == f'User type ({user_type}) does not exist'

    #CT5
    def test_is_valid_user_type_ct5(self):
        user_type = 'wrong_user'
        user_model = models.UniversityUser

        with pytest.raises(Exception) as error:
            UserType.is_valid_user_type(user_type, user_model)

            assert error == f'Wrong User type ({user_type}) for this Model User ({user_model})'

    #CT6
    def test_is_valid_user_type_ct6(self):
        user_type = 'wrong_user'
        user_model = models.CustomUser

        with pytest.raises(Exception) as error:
            UserType.is_valid_user_type(user_type, user_model)

    #Testes extras
    def test_get_user_type(self):
        user_type = 'super_user'
        result = UserType.get_user_type(user_type)

        assert result == user_type

    def test_get_user_type_false(self):
        user_type = 'false_user'
        with pytest.raises(Exception) as error:
            UserType.get_user_type(user_type)

            assert error == f'User type ({user_type}) does not exist'
            assert error == f'User type ({user_type}) does not exist'