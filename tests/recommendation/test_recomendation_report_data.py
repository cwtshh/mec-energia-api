import pytest
from recommendation.calculator import RecommendationCalculator
from tests.recommendation.test_cases import test_cases


test_data = [(code) for code in test_cases.keys()]

test_data = [(code) for code in test_cases.keys()]

@pytest
@pytest.mark.parametrize('code', test_data)
def test_recommendation_report_data(code: str):
    data = test_cases[code]
    sut = RecommendationCalculator(
        data.consumption_history,
        data.current_tariff_flag,
        data.blue_tariff,
        data.green_tariff,
    )

    result = sut.calculate()

    print(result)

test_recommendation_report_data('test_case_1')

