import pytest
from model import RelatorioRecomendacoes

mock_data_report = {
    'consumption_history': [
        ['2021-01-01', 1, 2, 3, 4, 5, 6, 7, 8],
        ['2021-02-01', 1, 2, 3, 4, 5, 6, 7, 8],
        ['2021-03-01', 1, 2, 3, 4, 5, 6, 7, 8],
    ],
    'recommendation': [
        {
            'date': '2021-01-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,
        },
        {
            'date': '2021-02-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,
        },
        {
            'date': '2021-03-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,	
        }

    ],
    'current_contract': [
        ['2021-01-01', 1, 2, 3, 4, 5],
        ['2021-02-01', 1, 2, 3, 4, 5],
        ['2021-03-01', 1, 2, 3, 4, 5],
    ],
    'pending_bills_dates': [
        '2021-04-01',
        '2021-05-01',
        '2021-06-01',
    ],
    'data_atualizacao': '2021-07-01',	
}

mock_uc = {
    'cnpj': '58577114000189',
    'name': 'UNIVERSIDADE DE BRASILIAs',
    'address': 'Campus Universitario Darcy Ribeiro - Asa Norte - Brasilia - DF - 70910900',
    'phone': '6133078000',
    'email': ''
}

mock_uc1 = {
    'cnpj': '58577114000189',
    'name': 'asdasd',
    'address': 'Campus Universitario Darcy Ribeiro - Asa Norte - Brasilia - DF - 70910900',
    'phone': '6133078000',
    'email': ''
}

mock_report = RelatorioRecomendacoes(
    uc=mock_uc,
    dados_relatorio=mock_data_report,
    data_atualizacao=mock_data_report['data_atualizacao']
)

def test_report_fields():
    try:
        assert mock_report.uc['name'] == mock_uc['name']
        assert mock_report.dados_relatorio == mock_data_report
        assert mock_report.data_atualizacao == mock_data_report['data_atualizacao']

        print('TEST OK!')
        print("All fields passed")

    except:
        raise AssertionError('Some fields are not right')

test_report_fields()


def test_report_data_calculator():
    mock_report_data = mock_report.generate_report()

    try:
        assert mock_report_data['total_consumption'] == 96
        assert mock_report_data['uc'] == mock_uc
        assert mock_report_data['data_atualizacao'] == mock_data_report['data_atualizacao']

        print('TEST OK!')
        print("All fields passed")
    except:
        raise AssertionError('total_consumption is not equal to 36')


test_report_data_calculator()

