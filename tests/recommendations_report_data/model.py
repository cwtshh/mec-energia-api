mock_data_report = {
    'consumption_history': [
        ['2021-01-01', 1, 2, 3, 4, 5, 6, 7, 8],
        ['2021-02-01', 1, 2, 3, 4, 5, 6, 7, 8],
        ['2021-03-01', 1, 2, 3, 4, 5, 6, 7, 8],
    ],
    'recommendation': [
        {
            'date': '2021-01-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,
        },
        {
            'date': '2021-02-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,
        },
        {
            'date': '2021-03-01',
            'blue_tariff': 1,
            'green_tariff': 2,
            'current_tariff': 3,
            'blue_tariff_consumption': 4,
            'green_tariff_consumption': 5,
            'current_tariff_consumption': 6,	
        }

    ],
    'current_contract': [
        ['2021-01-01', 1, 2, 3, 4, 5],
        ['2021-02-01', 1, 2, 3, 4, 5],
        ['2021-03-01', 1, 2, 3, 4, 5],
    ],
    'pending_bills_dates': [
        '2021-04-01',
        '2021-05-01',
        '2021-06-01',
    ],
    'data_atualizacao': '2021-07-01',	
}

class RelatorioRecomendacoes():
    def __init__(self, uc, dados_relatorio, data_atualizacao) -> None:
        self.uc = uc
        self.dados_relatorio = dados_relatorio
        self.data_atualizacao = data_atualizacao

    def generate_report(self):
        total_consumption = 0
        for recommendation in self.dados_relatorio['recommendation']:
            blue_tariff = recommendation['blue_tariff'] * recommendation['blue_tariff_consumption']
            green_tariff = recommendation['green_tariff'] * recommendation['green_tariff_consumption']
            current_tariff = recommendation['current_tariff'] * recommendation['current_tariff_consumption']

            total_consumption += blue_tariff + green_tariff + current_tariff

        return {
            'uc': self.uc,
            'total_consumption': total_consumption,
            'data_atualizacao': self.data_atualizacao
        }

            
    
    
